data class RecordListResponse(
    val records: List<Record>? = null
) : Response() {

    data class Record(
        val id: String,
        val value: String,
        val type: String,
        val line: String,
        val line_id: String,
    )
}