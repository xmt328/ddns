open class Response{
    val status: Status? = null

    class Status(
        val code: Int = 1,
        val message: String? = null,
        val created_at: String? = null,
    )
}