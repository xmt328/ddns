# DDNS

#### 介绍
一个 kotlin 的 DDNS 脚本, 支持 DnsPod

#### 使用说明

1. 参照 config.example.properties 编写 config.properties
2. 执行 ./ddns [config file], 默认为当前目录下的config.properties


#### 定时执行

- crontab */5 * * * * cd ddns/bin/ && ./ddns

#### 构建

-  ./gradlew distZip
